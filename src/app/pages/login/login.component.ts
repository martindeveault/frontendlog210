import { Component } from '@angular/core';
import { LoginService } from '../shared/services/login.service';
import { Router } from '@angular/router';

@Component({
	selector : 'login-component',
	template : require('./login.component.html'),
	styleUrls: ['./login.component.scss']
})

export class LoginComponent {
  public email: string = '';
  public password: string = '';

  constructor (public loginService: LoginService, private router:Router) {}

  public login() {
      this.loginService.login(this.email,this.password);
  }
}
