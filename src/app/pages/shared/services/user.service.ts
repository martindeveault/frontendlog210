import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class UserService {
  public userId: string;

  constructor (public http:Http) {}

  public getUsers(id: string) {
    return this.http.post(
      'https://fast-waters-15191.herokuapp.com/utilisateurs',
      JSON.stringify({
        uuid:id
      })
    ).toPromise();
  }
}
