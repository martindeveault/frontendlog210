import { Injectable } from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';

@Injectable()
export class LoginService {
  public userId: string;

  constructor (public http:Http) {}

  public login(email: string, password: string) {
    let headers = new Headers({ 'Access-Control-Allow-Origin': '*' });
    let options = new RequestOptions({ headers: headers });
      this.http.post(
        'https://fast-waters-15191.herokuapp.com/login',
        JSON.stringify({
          email: email,
          motdepasse: password
        })
      ).toPromise()
        .then( data => this.saveCurrentUser(data))
        .catch(data => console.log(data.status));
  }

  public saveCurrentUser(response: any) {
    this.userId = response['results'].uuid;
  }
}
