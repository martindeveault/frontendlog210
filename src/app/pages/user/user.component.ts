import { Component } from '@angular/core';
import { LoginService } from '../shared/services/login.service';
import { UserService } from '../shared/services/user.service';
import { Router } from '@angular/router';

@Component({
	selector : 'user-component',
	template : require('./user.component.html'),
	styleUrls: ['./user.component.scss']
})

export class UserComponent {
  public users: any[];

  constructor (public loginService: LoginService, private router:Router, public userService: UserService) {
    this.users = [{nom: 'Franco', prenom: 'Maxime', email: 'test@test.ca'}];
  }

  public login() {
      this.userService.getUsers(this.loginService.userId)
        .then(data => this.users = data['results']);
  }
}
